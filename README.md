README

# ◦ Requirements for the usage

- PHP 8.0+
- [Composer](https://getcomposer.org/doc/00-intro.md)
- Linux (Docker daemon might be used on any OS: Windows & macOS)

# ◦ Requirements for the package development

-  [docker-compose](https://docs.docker.com/compose/install/)

---

Recommended package for setting up a docker container: [prophp/docker-bridge](https://gitlab.com/prophp/docker-bridge)
([docker-compose](https://docs.docker.com/compose/install/) is required)

# ◦ Install

```sh
composer require prophp/bin-php --dev
```
---

### ▸ Initialize `bin/_php` directory
```sh
vendor/bin/php init
```

### `OPTIONAL`

Rename `bin/_php/config/var-www-html.dist.txt` to `bin/_php/config/var-www-html.txt` (Remove `.dist`)

Put your project directory's `absolute path` there, do not forget to put `/` at the end 

This config file will replace the substring `/var/www/html/` (path in docker image) with its contents when displaying an executed PHP file's `absolute path`


---

### ▸ Build a symlink `bin/example` for the file `bin/_php/src/example.php`

```sh
vendor/bin/php sym
```

Now you are ready to use `bin/example` executable SYMLINK, try it:

```sh
bin/example
```

Expected output:

```
Executing PHP file://<bin/_php/config/var-www-html.txt contents>bin/_php/src/example.php
Hello world!
```

---

### ▸ Build a php execution file `bin/_php/execute-all.php`
```sh
vendor/bin/php exec
```

Now you can use it in order to execute all files in `bin/_php/src/` directory. Useful for `Gitlab CI`

```sh
php bin/_php/execute-all.php
```

Expected output:

```
Executing PHP file://<bin/_php/config/var-www-html.txt contents>bin/_php/src/example.php
Hello world!

```

---

### ▸ GIT commit `bin` directory excluding `bin/_php/config` one

`.gitignore`

```
/bin/_php/config/*
```


`bin/example` && `php bin/_php/execute-all.php` will be working even if `prophp/bin-php` package is removed from Composer (`vendor` dir)

You may try it by using this composer command:

```sh
composer remove prophp/bin-php --dev
```

Do not forget to install it afterwards for using its DEV tools


# ◦ Usage

### ▸ Create a new PHP file that you want to execute using `bin/<symlink>`

You may create any `.php` files and subdirectories for them inside `bin/_php/src` directory

Take a look at `/bin/_php/src/example.php`

---

### ▸ Rebuild symlinks for all `.php` files inside `bin/_php/src` directory

```sh
vendor/bin/php sym
```

---

### ▸ Rebuild a `bin/_php/execute-all.php` file that will execute all `.php` files inside `bin/_php/src/` directory

```sh
vendor/bin/php exec
```


# ◦ Usage example

### ▸ Create a new PHP file that you want to execute using `bin/<symlink>`

Create a `bin/_php/src/create/products.php` file

```php
#!/usr/bin/env php
<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";

echo "Create products!" . PHP_EOL;
```

### ▸ Rebuild symlinks

```sh
vendor/bin/php sym
```

### ▸ Use the freshly generated symbolic link. You are free to use autosuggestions pressing `Tab` button

```sh
bin/create/products
```

### ▸ Rebuild a file that executes all your `.php` files inside `bin/_php/src`

```sh
vendor/bin/php exec
```

Try it:

```sh
php bin/_php/execute-all.php
```

Expected output:

```
Executing PHP file://<bin/_php/config/var-www-html.txt contents>bin/_php/src/example.php
Hello world!

Executing PHP file://<bin/_php/config/var-www-html.txt contents>bin/_php/src/create/products.php
Create products!

```

# ◦ GitLab CI

When using a `GitLab CI`, you need to execute PHP files directly as `GitLab CI` has problems with SYMLINKS (at the moment)

If you use SYMLINKS, `GitLab CI` will execute them properly but will not mark tests as `FAILED` even if your `.php` file throws an Exception. (Tests will always succeed)

#### You have to use `php` for proper results!

You may specify certain scripts manually

`.gitlab-ci.yml`

```
test:8.0:
  script:
    - exec "php bin/_php/src/example.php"
    - exec "php bin/_php/src/create/products.php"
```

or use a freshly generated `bin/_php/execute-all.php` file to execute them all at once

`.gitlab-ci.yml`

```
test:8.0:
  script:
    - exec "php bin/_php/execute-all.php"
```
