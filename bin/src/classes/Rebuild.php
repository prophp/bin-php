<?php

class Rebuild
{
    private function globRecursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->globRecursive($dir . '/' . basename($pattern), $flags));
        }
        return $files;
    }

    public function symlinks()
    {
        foreach ($this->globRecursive(getcwd() . "/bin/_php/src/*.php") as $filePath) {

            $symlinkPath = substr(
                str_replace(getcwd() . "/bin/_php/src/", '', $filePath),
                0, -4
            );

            $symlinkDirPath = dirname($symlinkPath) . "/";

            if($symlinkDirPath !== "./"){

                exec("mkdir -p \"bin/$symlinkDirPath\"");

                $relativeDirPrefix = str_repeat("../", substr_count($symlinkDirPath, '/'));

                $symlinkBasename = basename($symlinkPath);

                exec(
                    "
                cd bin/$symlinkDirPath && \
                ln -sf \"{$relativeDirPrefix}_php/proxy\" \"$symlinkBasename\" && \
                chmod +x \"$symlinkBasename\"
                "
                );

                continue;
            }

            exec(
                "
                cd bin && \
                ln -sf \"_php/proxy\" \"$symlinkPath\" && \
                chmod +x \"$symlinkPath\"
                "
            );
        }
    }

    public function executionFile()
    {
        $varWwwHtmlFilePath = dirname(__DIR__, 6) . "/bin/_php/config/var-www-html.txt";

        $filePaths = [];

        foreach ($this->globRecursive(dirname(__DIR__, 6) . "/bin/_php/src/*.php") as $filePath) {
            $filePaths[] = $filePath;
        }

        $contents = "";

        foreach ($filePaths as $filePath){
            $filePathsReference = str_replace('/var/www/html/', file_get_contents($varWwwHtmlFilePath), $filePath);
            $contents .= "Executing PHP file://$filePathsReference\n" .
                "<?php require_once \"$filePath\"; ?>\n\n";
        }

        file_put_contents(dirname(__DIR__, 6) . "/bin/_php/execute-all.php", $contents);
    }
}