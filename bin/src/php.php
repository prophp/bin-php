#!/usr/bin/env php
<?php

require_once __DIR__ . "/classes/Rebuild.php";

if (count($argv) <= 1) {
    echo "No argument provided for 'vendor/bin/php $1'" . PHP_EOL .
        "init (Initialize)\nsym (Rebuild symlinks)\nexec (Rebuild PHP auto execution file)" . PHP_EOL;
}

if (count($argv) === 2) {
    switch ($argv[1]) {
        case "init":
            // @todo Improve, should be used from any location, not only root dir
            $PWD = getcwd();
            exec("cp -a $PWD/vendor/prophp/bin-php/copy/. \"$PWD\"");
            break;
        case "sym":
            (new Rebuild())->symlinks();
            break;
        case "exec":
            (new Rebuild())->executionFile();
            break;
        default:
            throw new Exception("Unknown $1 parameter value '{$argv[1]}'");
    }
}
